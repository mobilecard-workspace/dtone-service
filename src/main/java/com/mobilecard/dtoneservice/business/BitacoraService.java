package com.mobilecard.dtoneservice.business;

import com.mobilecard.dtoneservice.model.TBitacora;
import com.mobilecard.dtoneservice.repository.TBitacoraRepository;
import com.mobilecard.dtoneservice.request.RequestTransacction;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

@Service
@Log4j2
public class BitacoraService {

    /**
     * Repository to sabe T_BITACORA
     */
    @Autowired
    private TBitacoraRepository tBitacoraRepository;

    // #### CONSTANTES ####
    private static final String ID_PROVEEDOR = "700";
    private static final BigInteger ID_PRODUCTO = new BigInteger("7100");

    public TBitacora saveTBitacora(RequestTransacction requestTransacction, Integer idApp, Integer idPais, String idioma) {
        log.info("Guardando bitacora en t_bitacora...");
        TBitacora tBitacora = new TBitacora();
        try {
            tBitacora.setIdUsuario(requestTransacction.getIdUsuario());
            tBitacora.setIdAplicacion(idApp);
            tBitacora.setIdioma(idioma);
            tBitacora.setIdProveedor(ID_PROVEEDOR);
            tBitacora.setIdProducto(ID_PRODUCTO);
            tBitacora.setBitFecha(new Date());
            tBitacora.setBitHora(new Timestamp(new Date().getTime()));
            tBitacora.setBitConcepto(""); //TODO preguntar
            tBitacora.setBitCargo(requestTransacction.getSource().getAmount() == null ? BigDecimal.ZERO : requestTransacction.getSource().getAmount());
            tBitacora.setBitComision(BigDecimal.ZERO);
            tBitacora.setBitCardId(0); //TODO preguntar
            tBitacora.setIdPais(idPais);
            tBitacora.setIdEstablecimiento(requestTransacction.getIdEstablecimiento() == null ? BigInteger.ZERO : requestTransacction.getIdEstablecimiento());
            tBitacoraRepository.save(tBitacora);
            log.info("Se guardo con EXITO la bitacora t_bitacora: {}", tBitacora.toString());
            return tBitacora;
        } catch (RuntimeException ex) {
            log.error("Ocurrio un error al guardar en T_BITACORA, mensaje: " + ex.getMessage(), ex);
            return null;
        }
    }

    public TBitacora updateTBitacora(TBitacora tBitacora) {
        return tBitacoraRepository.save(tBitacora);
    }
}
