package com.mobilecard.dtoneservice.business;

import com.mobilecard.dtoneservice.client.DtoneApiClientImpl;
import com.mobilecard.dtoneservice.client.PathsClient;
import com.mobilecard.dtoneservice.client.request.transaction.*;
import com.mobilecard.dtoneservice.client.request.transaction.Destination;
import com.mobilecard.dtoneservice.client.request.transaction.Source;
import com.mobilecard.dtoneservice.client.response.*;
import com.mobilecard.dtoneservice.client.response.transaction.TransactionResponse;
import com.mobilecard.dtoneservice.exception.ClientException;
import com.mobilecard.dtoneservice.request.RequestTransacction;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@Log4j2
public class DtOneBusiness {

    @Autowired
    private DtoneApiClientImpl<ServiceResponse> clientService;

    @Autowired
    private DtoneApiClientImpl<CountriesResponse> clientCountry;

    @Autowired
    private DtoneApiClientImpl<OperatorResponse> clientOperatos;

    @Autowired
    private DtoneApiClientImpl<BenefitsResponse> clientBenefits;

    @Autowired
    private DtoneApiClientImpl<PromotionsResponse> clientPromotions;

    @Autowired
    private DtoneApiClientImpl<ProductsResponse> clientProductos;

    @Autowired
    private DtoneApiClientImpl<TransactionResponse> clientTransaction;

    @Autowired
    private BitacoraService bitacoraService;

    /**
     * Get list service
     */
    public ResponseEntity getServices() {
        log.info("exec getServices");
        try {
            ResponseEntity responseDtOneService = clientService.getList(PathsClient.SERVICE_PATH);
            if (HttpStatus.OK.equals(responseDtOneService.getStatusCode())) {
                log.info("Http petition GET service response OK");
                return new ResponseEntity(responseDtOneService.getBody(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (ClientException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get list service
     */
    public ResponseEntity getServices(String serviceId) {
        log.info("exec getServices");
        try {
            StringBuilder path = new StringBuilder(PathsClient.SERVICE_PATH).append("/").append(serviceId);
            ResponseEntity responseDtOneService = clientService.getObject(path.toString());
            if (HttpStatus.OK.equals(responseDtOneService.getStatusCode())) {
                log.info("Http petition GET service response OK");
                return new ResponseEntity(responseDtOneService.getBody(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (ClientException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get list service
     */
    public ResponseEntity getCountries() {
        log.info("exec getCountries");
        try {
            ResponseEntity responseDtOneService = clientCountry.getList(PathsClient.COUNTRIES);
            if (HttpStatus.OK.equals(responseDtOneService.getStatusCode())) {
                log.info("Http petition GET countries response OK");
                return new ResponseEntity(responseDtOneService.getBody(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (ClientException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get list service
     */
    public ResponseEntity getCountries(String countryIsoCode) {
        log.info("exec getCountries");
        try {
            StringBuilder path = new StringBuilder(PathsClient.COUNTRIES).append("/").append(countryIsoCode);
            ResponseEntity responseDtOneService = clientCountry.getObject(path.toString());
            if (HttpStatus.OK.equals(responseDtOneService.getStatusCode())) {
                log.info("Http petition GET countries response OK");
                return new ResponseEntity(responseDtOneService.getBody(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (ClientException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get list service
     */
    public ResponseEntity getOperatos() {
        log.info("exec getOperatos");
        try {
            ResponseEntity responseDtOneService = clientOperatos.getList(PathsClient.OPERATORS);
            if (HttpStatus.OK.equals(responseDtOneService.getStatusCode())) {
                log.info("Http petition GET service response OK");
                return new ResponseEntity(responseDtOneService.getBody(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (ClientException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get list service
     */
    public ResponseEntity getOperatos(String operatorId) {
        log.info("exec getOperatos");
        try {
            StringBuilder path = new StringBuilder(PathsClient.OPERATORS).append("/").append(operatorId);
            ResponseEntity responseDtOneService = clientOperatos.getObject(path.toString());
            if (HttpStatus.OK.equals(responseDtOneService.getStatusCode())) {
                log.info("Http petition GET service response OK");
                return new ResponseEntity(responseDtOneService.getBody(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (ClientException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get list service
     */
    public ResponseEntity getBenefits() {
        log.info("exec getBenefits");
        try {
            ResponseEntity responseDtOneService = clientBenefits.getList(PathsClient.BENEFITS);
            if (HttpStatus.OK.equals(responseDtOneService.getStatusCode())) {
                log.info("Http petition GET service response OK");
                return new ResponseEntity(responseDtOneService.getBody(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (ClientException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get list service
     */
    public ResponseEntity getPromotions() {
        log.info("exec getPromotions");
        try {
            ResponseEntity responseDtOneService = clientPromotions.getList(PathsClient.PROMOTIONS);
            if (HttpStatus.OK.equals(responseDtOneService.getStatusCode())) {
                log.info("Http petition GET service response OK");
                return new ResponseEntity(responseDtOneService.getBody(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (ClientException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get list service
     */
    public ResponseEntity getPromotions(String promotionId) {
        log.info("exec getPromotions");
        try {
            StringBuilder path = new StringBuilder(PathsClient.PROMOTIONS).append("/").append(promotionId);
            ResponseEntity responseDtOneService = clientPromotions.getObject(path.toString());
            if (HttpStatus.OK.equals(responseDtOneService.getStatusCode())) {
                log.info("Http petition GET service response OK");
                return new ResponseEntity(responseDtOneService.getBody(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (ClientException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get list service
     */
    public ResponseEntity getProducts() {
        log.info("exec getProducts");
        try {
            ResponseEntity responseDtOneService = clientProductos.getList(PathsClient.PRODUCTS);
            if (HttpStatus.OK.equals(responseDtOneService.getStatusCode())) {
                log.info("Http petition GET service response OK");
                return new ResponseEntity(responseDtOneService.getBody(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (ClientException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get list service
     */
    public ResponseEntity getProducts(String productId) {
        log.info("exec getProducts");
        try {
            StringBuilder path = new StringBuilder(PathsClient.PRODUCTS).append("/").append(productId);
            ResponseEntity responseDtOneService = clientProductos.getObject(path.toString());
            if (HttpStatus.OK.equals(responseDtOneService.getStatusCode())) {
                log.info("Http petition GET service response OK");
                return new ResponseEntity(responseDtOneService.getBody(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (ClientException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Create a transaction
     */
    public ResponseEntity doTransaction(RequestTransacction requestTransacction, Integer idApp, Integer idPais, String idioma) {
        log.info("exec doTransaction, request: {}", requestTransacction.toString());
        try {
            bitacoraService.saveTBitacora(requestTransacction, idApp, idPais, idioma);
            ResponseEntity<TransactionResponse> response = clientTransaction.postTransaction(PathsClient.ASYNC_TRANSACTION, createRequestClient(requestTransacction));
            return response;
        } catch (ClientException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    private com.mobilecard.dtoneservice.client.request.transaction.RequestTransacction createRequestClient(RequestTransacction requestTransacction) {
        com.mobilecard.dtoneservice.client.request.transaction.RequestTransacction request = new com.mobilecard.dtoneservice.client.request.transaction.RequestTransacction();
        request.setExternalId(LocalDateTime.now().toString());
        request.setPruductId(requestTransacction.getPruductId());
        request.setCalculationMode(requestTransacction.getCalculationMode());
        Source source = new Source();
        source.setAmount(requestTransacction.getSource().getAmount());
        source.setUnit(requestTransacction.getSource().getUnit());
        request.setSource(source);
        Destination destination = new Destination();
        destination.setAmount(requestTransacction.getDestination().getAmount());
        destination.setUnit(requestTransacction.getDestination().getUnit());
        request.setDestination(destination);
        request.setAutoConfirm(requestTransacction.isAutoConfirm());
        Sender sender = new Sender();
        sender.setEmail(requestTransacction.getSender().getEmail());
        sender.setFirstName(requestTransacction.getSender().getFirstName());
        sender.setLastName(requestTransacction.getSender().getLastName());
        sender.setMiddleName(requestTransacction.getSender().getMiddleName());
        sender.setMobileNumber(requestTransacction.getSender().getMobileNumber());
        sender.setNationalityCountryIsoCode(requestTransacction.getSender().getNationalityCountryIsoCode());
        request.setSender(sender);
        Beneficiary beneficiary = new Beneficiary();
        beneficiary.setEmail(requestTransacction.getBeneficiary().getEmail());
        beneficiary.setFirstName(requestTransacction.getBeneficiary().getFirstName());
        beneficiary.setLastName(requestTransacction.getBeneficiary().getLastName());
        beneficiary.setMiddleName(requestTransacction.getBeneficiary().getMiddleName());
        beneficiary.setMobileNumber(requestTransacction.getBeneficiary().getMobileNumber());
        beneficiary.setNationalityCountryIsoCode(requestTransacction.getBeneficiary().getNationalityCountryIsoCode());
        request.setBeneficiary(beneficiary);
        DebitPartyIdentifier debitPartyIdentifier = new DebitPartyIdentifier();
        debitPartyIdentifier.setAccountNumber(requestTransacction.getDebitPartyIdentifier().getAccountNumber());
        debitPartyIdentifier.setMobileNumber(requestTransacction.getDebitPartyIdentifier().getMobileNumber());
        request.setDebitPartyIdentifier(debitPartyIdentifier);
        CreditPartyIdentifier creditPartyIdentifier = new CreditPartyIdentifier();
        creditPartyIdentifier.setAccountNumber(requestTransacction.getCreditPartyIdentifier().getAccountNumber());
        creditPartyIdentifier.setMobileNumber(requestTransacction.getCreditPartyIdentifier().getMobileNumber());
        request.setCreditPartyIdentifier(creditPartyIdentifier);
        request.setCallbackUrl(requestTransacction.getCallbackUrl());
        log.info("REQUEST API DTONE - TRANSACTION: " + request.toString());
        return request;
    }
}
