package com.mobilecard.dtoneservice.controller;

import com.mobilecard.dtoneservice.business.DtOneBusiness;
import com.mobilecard.dtoneservice.request.RequestTransacction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TransactionController {

    @Autowired
    private DtOneBusiness dtOneBusiness;

    /**
     * POST Transaction
     */
    @PostMapping(value = "{idApp}/{idPais}/{idioma}/transaction")
    public ResponseEntity postTransaction(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") Integer idPais,
            @PathVariable("idioma") String idioma,
            @RequestBody RequestTransacction requestTransacction
    ) {
        return dtOneBusiness.doTransaction(requestTransacction, idApp, idPais, idioma);
    }
}
