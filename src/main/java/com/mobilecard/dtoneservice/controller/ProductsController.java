package com.mobilecard.dtoneservice.controller;

import com.mobilecard.dtoneservice.business.DtOneBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductsController {

    @Autowired
    private DtOneBusiness dtOneBusiness;

    /**
     * Get list service
     */
    @GetMapping(value = "{idApp}/{idPais}/{idioma}/products")
    public ResponseEntity getProducts(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") Integer idPais,
            @PathVariable("idioma") String idioma
    ) {
        return dtOneBusiness.getProducts();
    }

    @GetMapping("{idApp}/{idPais}/{idioma}/products/{productId}")
    public ResponseEntity getProductsById(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") Integer idPais,
            @PathVariable("idioma") String idioma,
            @PathVariable("productId") String productId) {
        return dtOneBusiness.getProducts(productId);
    }
}
