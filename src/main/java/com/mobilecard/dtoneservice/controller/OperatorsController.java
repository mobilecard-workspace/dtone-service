package com.mobilecard.dtoneservice.controller;

import com.mobilecard.dtoneservice.business.DtOneBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OperatorsController {

    @Autowired
    private DtOneBusiness dtOneBusiness;

    /**
     * Get list service
     */
    @GetMapping(value = "{idApp}/{idPais}/{idioma}/operators")
    public ResponseEntity getOperator(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") Integer idPais,
            @PathVariable("idioma") String idioma
    ) {
        return dtOneBusiness.getOperatos();
    }

    @GetMapping("{idApp}/{idPais}/{idioma}/operators/{operatorId}")
    public ResponseEntity getOperatorById(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") Integer idPais,
            @PathVariable("idioma") String idioma,
            @PathVariable("operatorId") String operatorId){
        return dtOneBusiness.getOperatos(operatorId);
    }
}
