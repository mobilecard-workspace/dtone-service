package com.mobilecard.dtoneservice.controller;

import com.mobilecard.dtoneservice.business.DtOneBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountryController {

    @Autowired
    private DtOneBusiness dtOneBusiness;

    /**
     * Get list service
     */
    @GetMapping(value = "{idApp}/{idPais}/{idioma}/countries")
    public ResponseEntity getCountries(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") Integer idPais,
            @PathVariable("idioma") String idioma
    ) {
        return dtOneBusiness.getCountries();
    }

    /**
     * Get list service
     */
    @GetMapping("{idApp}/{idPais}/{idioma}/countries/{countryIsoCode}")
    public ResponseEntity getCountries(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") Integer idPais,
            @PathVariable("idioma") String idioma,
            @PathVariable(value = "countryIsoCode") String countryIsoCode) {
        return dtOneBusiness.getCountries(countryIsoCode);
    }
}
