package com.mobilecard.dtoneservice.controller;

import com.mobilecard.dtoneservice.business.DtOneBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PromotionsController {

    @Autowired
    private DtOneBusiness dtOneBusiness;

    /**
     * Get list service
     */
    @GetMapping(value = "{idApp}/{idPais}/{idioma}/promotions")
    public ResponseEntity getPromotions(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") Integer idPais,
            @PathVariable("idioma") String idioma
    ) {
        return dtOneBusiness.getPromotions();
    }

    @GetMapping("{idApp}/{idPais}/{idioma}/promotions/{promotionId}")
    public ResponseEntity getPromotionById(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") Integer idPais,
            @PathVariable("idioma") String idioma,
            @PathVariable("promotionId") String promotionId){
        return dtOneBusiness.getPromotions(promotionId);
    }
}
