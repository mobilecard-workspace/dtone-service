package com.mobilecard.dtoneservice;

import com.mobilecard.dtoneservice.configuration.KeysConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class DtoneServiceApplication extends SpringBootServletInitializer {

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;
    @Autowired
    private KeysConfiguration keysConfiguration;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DtoneServiceApplication.class);
    }


    public static void main(String[] args) {
        SpringApplication.run(DtoneServiceApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return restTemplateBuilder.basicAuthentication(keysConfiguration.getApiKey(), keysConfiguration.getApiSecret()).build();
    }
}
