package com.mobilecard.dtoneservice.repository;

import com.mobilecard.dtoneservice.model.TBitacora;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TBitacoraRepository extends CrudRepository<TBitacora, Integer> {
}
