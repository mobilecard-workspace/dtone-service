package com.mobilecard.dtoneservice.request;

import lombok.Data;

@Data
public class CreditPartyIdentifier {
    private String mobileNumber;
    private String accountNumber;

}
