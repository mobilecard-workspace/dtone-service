package com.mobilecard.dtoneservice.request;

import lombok.Data;

import java.math.BigInteger;

@Data
public class RequestTransacction {
    private Integer pruductId;
    private CalculationMode calculationMode;
    private Source source;
    private Destination destination;
    private boolean autoConfirm;
    private Sender sender;
    private Beneficiary beneficiary;
    private DebitPartyIdentifier debitPartyIdentifier;
    private CreditPartyIdentifier creditPartyIdentifier;
    private String callbackUrl;

    //Parametros para t_bitacora
    private BigInteger idUsuario;
    private BigInteger idEstablecimiento;
}
