package com.mobilecard.dtoneservice.request;

import lombok.Data;

@Data
public class Sender {
    private String lastName;
    private String firstName;
    private String middleName;
    private String nationalityCountryIsoCode;
    private String mobileNumber;
    private String email;
}
