package com.mobilecard.dtoneservice.request;

import lombok.Data;

@Data
public class Beneficiary {
    private String lastName;
    private String firstName;
    private String middleName;
    private String nationalityCountryIsoCode;
    private String mobileNumber;
    private String email;
}
