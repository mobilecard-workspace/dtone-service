package com.mobilecard.dtoneservice.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Destination {
    private String unit;
    private BigDecimal amount;
}
