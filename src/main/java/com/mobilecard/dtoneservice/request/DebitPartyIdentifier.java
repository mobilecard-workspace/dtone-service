package com.mobilecard.dtoneservice.request;

import lombok.Data;

@Data
public class DebitPartyIdentifier {
    private String mobileNumber;
    private String accountNumber;
}
