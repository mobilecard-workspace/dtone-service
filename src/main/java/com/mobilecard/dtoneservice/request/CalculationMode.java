package com.mobilecard.dtoneservice.request;

public enum CalculationMode {
    SOURCE_AMOUNT,
    DESTINATION_AMOUNT
    ;
}
