package com.mobilecard.dtoneservice.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "com.mobilecard.dtoneservice.client")
@Component
public class KeysConfiguration {
    private String apiKey;
    private String apiSecret;
}
