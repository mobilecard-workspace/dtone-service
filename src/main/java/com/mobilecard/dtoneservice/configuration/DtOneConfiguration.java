package com.mobilecard.dtoneservice.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "com.mobilecard.dtoneservice.client.config")
@Component
public class DtOneConfiguration {
    private String preProductionUrl;
}
