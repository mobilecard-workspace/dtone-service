package com.mobilecard.dtoneservice.client;

public interface PathsClient {
    String SERVICE_PATH = "/services";

    String COUNTRIES = "/countries";

    String OPERATORS = "/operators";

    String BENEFITS = "/benefit-types";

    String PROMOTIONS = "/promotions";

    String PRODUCTS = "/products";

    String ASYNC_TRANSACTION = "/async/transactions";
}
