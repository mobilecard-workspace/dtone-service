package com.mobilecard.dtoneservice.client;

import com.mobilecard.dtoneservice.configuration.KeysConfiguration;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.Base64;

@Component
@Log4j2
public class ClientUtil {

    @Autowired
    private KeysConfiguration keysConfiguration;

    private static final String LOG_HTTP_STATUS = "Http status: {}";
    private static final String LOG_HTTP_BODY = "Body response: {}";

    /**
     * Get Header with authorizations
     */
    public HttpHeaders getHeadersWithAuthBasic() {
        return new HttpHeaders() {{
            String auth = keysConfiguration.getApiKey() + ":" + keysConfiguration.getApiSecret();
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes());
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }

    public void writeLogClientException(HttpClientErrorException clientEx){
        log.error("HTTP Client error", clientEx);
        log.info(LOG_HTTP_STATUS, clientEx.getStatusText());
        log.info(LOG_HTTP_BODY, clientEx.getResponseBodyAsString());
    }

    public void writeLogServerException(HttpServerErrorException serverEx){
        log.error("HTTP Server error", serverEx);
        log.info(LOG_HTTP_STATUS, serverEx.getStatusText());
        log.info(LOG_HTTP_BODY, serverEx.getResponseBodyAsString());
    }
}
