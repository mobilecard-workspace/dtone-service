package com.mobilecard.dtoneservice.client;

import com.mobilecard.dtoneservice.client.request.transaction.RequestTransacction;
import com.mobilecard.dtoneservice.client.response.transaction.TransactionResponse;
import com.mobilecard.dtoneservice.configuration.DtOneConfiguration;
import com.mobilecard.dtoneservice.exception.ClientException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Log4j2
@Service
public class DtoneApiClientImpl<T> implements DtoneApiClient {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private DtOneConfiguration dtOneConfiguration;

    @Autowired
    private ClientUtil clientUtil;


    @Override
    public ResponseEntity<List<T>> getList(String path) throws ClientException {
        log.info("GET {}", path);
        try {
            String url = dtOneConfiguration.getPreProductionUrl() + path;
            log.debug("URL: {}", url);
            ResponseEntity<List<T>> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<T>>() {
                    });
            return responseEntity;
        } catch (HttpClientErrorException clientEx) {
            clientUtil.writeLogClientException(clientEx);
            throw new ClientException();
        } catch (HttpServerErrorException serverEx) {
            clientUtil.writeLogServerException(serverEx);
            throw new ClientException();
        }
    }

    @Override
    public ResponseEntity getObject(String path) throws ClientException {
        log.info("GET {}", path);
        try {
            String url = dtOneConfiguration.getPreProductionUrl() + path;
            log.debug("URL: {}", url);
            ResponseEntity<T> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<T>() {
                    });
            return responseEntity;
        } catch (HttpClientErrorException clientEx) {
            clientUtil.writeLogClientException(clientEx);
            throw new ClientException();
        } catch (HttpServerErrorException serverEx) {
            clientUtil.writeLogServerException(serverEx);
            throw new ClientException();
        }
    }

    public ResponseEntity postTransaction(String path, RequestTransacction request) throws ClientException {
        log.info("POST {}", path);
        String url = dtOneConfiguration.getPreProductionUrl() + path;
        try {
            ResponseEntity<TransactionResponse> responseEntity = restTemplate.postForEntity(url, request, TransactionResponse.class);
            return responseEntity;
        } catch (HttpClientErrorException clientEx) {
            clientUtil.writeLogClientException(clientEx);
            throw new ClientException(clientEx.getResponseBodyAsString());
        } catch (HttpServerErrorException serverEx) {
            clientUtil.writeLogServerException(serverEx);
            throw new ClientException(serverEx.getMessage());
        }
    }
}
