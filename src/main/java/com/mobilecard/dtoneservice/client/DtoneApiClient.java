package com.mobilecard.dtoneservice.client;

import com.mobilecard.dtoneservice.exception.ClientException;
import org.springframework.http.ResponseEntity;

public interface DtoneApiClient {

    /**
     * Do GET response List
     */
    ResponseEntity getList(String path) throws ClientException;

    /**
     * Do get response Object
     */
    ResponseEntity getObject(String path) throws ClientException;
}
