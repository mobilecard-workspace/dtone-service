package com.mobilecard.dtoneservice.client.request.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Destination {
    @JsonProperty("unit_type")
    private String unitType = "CURRENCY";
    private String unit;
    private BigDecimal amount;
}
