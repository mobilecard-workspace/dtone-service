package com.mobilecard.dtoneservice.client.request.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DebitPartyIdentifier {
    @JsonProperty("mobile_number")
    private String mobileNumber;
    @JsonProperty("account_number")
    private String accountNumber;
}
