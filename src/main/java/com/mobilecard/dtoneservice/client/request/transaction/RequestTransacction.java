package com.mobilecard.dtoneservice.client.request.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mobilecard.dtoneservice.request.CalculationMode;
import lombok.Data;

@Data
public class RequestTransacction {
    @JsonProperty("external_id")
    private String externalId;
    @JsonProperty("product_id")
    private Integer pruductId;
    @JsonProperty("calculation_mode")
    private CalculationMode calculationMode;
    private Source source;
    private Destination destination;
    @JsonProperty("auto_confirm")
    private boolean autoConfirm;
    private Sender sender;
    private Beneficiary beneficiary;
    @JsonProperty("debit_party_identifier")
    private DebitPartyIdentifier debitPartyIdentifier;
    @JsonProperty("credit_party_identifier")
    private CreditPartyIdentifier creditPartyIdentifier;
    @JsonProperty("callback_url")
    private String callbackUrl;
}
