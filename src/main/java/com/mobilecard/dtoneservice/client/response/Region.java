package com.mobilecard.dtoneservice.client.response;

import lombok.Data;

@Data
public class Region {
    private String name;
    private String code;
}
