package com.mobilecard.dtoneservice.client.response.transaction;

import lombok.Data;

@Data
public class AdjustedValues {
    private Source source;
    private Destination destination;
}
