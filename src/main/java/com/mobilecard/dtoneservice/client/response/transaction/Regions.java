package com.mobilecard.dtoneservice.client.response.transaction;

import lombok.Data;

@Data
public class Regions {
    private String name;
    private String code;
}
