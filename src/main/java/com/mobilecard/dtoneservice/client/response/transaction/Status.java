package com.mobilecard.dtoneservice.client.response.transaction;

import lombok.Data;

@Data
public class Status {
    private Integer id;
    private String message;
    private Class aClass;
}
