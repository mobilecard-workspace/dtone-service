package com.mobilecard.dtoneservice.client.response;

import lombok.Data;

@Data
public class Prices {
    private Wholesale wholesale;
    private Retail retail;
}
