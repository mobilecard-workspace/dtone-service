package com.mobilecard.dtoneservice.client.response.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Country {
    private String name;
    @JsonProperty("iso_code")
    private String isoCode;
    private List<Regions> regions;
}
