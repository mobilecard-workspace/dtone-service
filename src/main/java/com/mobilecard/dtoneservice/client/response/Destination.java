package com.mobilecard.dtoneservice.client.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Destination {
    @JsonProperty("unit_type")
    private String unitType;
    private String unit;
    private Integer amount;
}
