package com.mobilecard.dtoneservice.client.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProductsResponse {
    private String id;
    private String name;
    private String description;
    private String type;
    private ServiceResponse serviceResponse;
    private List<Region> regions;
    @JsonProperty("required_debit_party_identifier_fields")
    private List<String> requiredDebitPartyIdentifierFields;
    @JsonProperty("required_credit_party_identifier_fields")
    private List<String> requiredCreditPartyIdentifierFields;
    @JsonProperty("required_sender_fields")
    private List<String> requiredSenderFields;
    @JsonProperty("required_beneficiary_fields")
    private List<String> requiredBeneficiaryFields;
    @JsonProperty("availability_zones")
    private List<String> availabilityZones;
    private Source source;
    private Prices prices;
    private Rates rates;
    private List<Benefits> benefits;
    private List<PromotionsResponse> promotions;
}
