package com.mobilecard.dtoneservice.client.response;

import lombok.Data;

@Data
public class BenefitsResponse {
    private String name;
}
