package com.mobilecard.dtoneservice.client.response.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigInteger;
import java.util.List;

@Data
public class TransactionResponse {
    private BigInteger id;
    @JsonProperty("external_id")
    private String externalId;
    @JsonProperty("creation_date")
    private String creationDate;
    @JsonProperty("confirmation_expiration_date")
    private String confirmationExpirationDate;
    @JsonProperty("confirmation_date")
    private String confirmationDate;
    private Status status;
    @JsonProperty("operator_reference")
    private String operatorReference;
    private Pin pin;
    private Product product;
    private Prices prices;
    private Rates rates;
    private List<Benefits> benefits;
    private List<Promotions> promotions;
    @JsonProperty("requested_values")
    private RequestedValues requestedValues;
    @JsonProperty("adjusted_values")
    private AdjustedValues adjustedValues;
    private Sender sender;
    private Beneficiary beneficiary;
    @JsonProperty("debit_party_identifier")
    private DebitPartyIdentifier debitPartyIdentifier;
    @JsonProperty("credit_party_identifier")
    private CreditPartyIdentifier creditPartyIdentifier;
}
