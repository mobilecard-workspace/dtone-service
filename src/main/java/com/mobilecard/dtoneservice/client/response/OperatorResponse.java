package com.mobilecard.dtoneservice.client.response;

import lombok.Data;

import java.util.List;

@Data
public class OperatorResponse {
    private String id;
    private String name;
    private CountriesResponse country;
    private List<Region> regions;

}
