package com.mobilecard.dtoneservice.client.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Benefits {
    private String type;
    @JsonProperty("unit_type")
    private String unitType;
    private String unit;
    private Amount amount;
    @JsonProperty("additional_information")
    private String additionalInformation;
}
