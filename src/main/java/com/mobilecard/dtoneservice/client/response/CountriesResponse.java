package com.mobilecard.dtoneservice.client.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CountriesResponse {
    private String name;
    @JsonProperty("iso_code")
    private String isoCode;
    private List<Region> regions;
}
