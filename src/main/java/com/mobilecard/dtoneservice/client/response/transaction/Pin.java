package com.mobilecard.dtoneservice.client.response.transaction;

import lombok.Data;

@Data
public class Pin {
    private String code;
    private String serial;
}
