package com.mobilecard.dtoneservice.client.response.transaction;

import lombok.Data;

import java.math.BigInteger;

@Data
public class Product {
    private BigInteger id;
    private String name;
    private String description;
    private String type;
    private Service service;
    private Operator operator;
}
