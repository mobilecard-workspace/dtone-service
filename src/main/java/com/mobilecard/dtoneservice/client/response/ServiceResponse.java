package com.mobilecard.dtoneservice.client.response;

import lombok.Data;

@Data
public class ServiceResponse {
    private Integer id;
    private String name;
}
