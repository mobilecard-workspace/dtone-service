package com.mobilecard.dtoneservice.client.response.transaction;

import lombok.Data;

@Data
public class Service {
    private Integer id;
    private String name;
}
