package com.mobilecard.dtoneservice.client.response;

import lombok.Data;

@Data
public class Operator {
    private String id;
    private String name;
    private CountriesResponse country;
}
