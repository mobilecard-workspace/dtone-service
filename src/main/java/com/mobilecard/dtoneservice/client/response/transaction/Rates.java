package com.mobilecard.dtoneservice.client.response.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Rates {
    private Integer base;
    private Integer wholesale;
    @JsonProperty("discount_from_base")
    private Integer discountFromBase;
}
