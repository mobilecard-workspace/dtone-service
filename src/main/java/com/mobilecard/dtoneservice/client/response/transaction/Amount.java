package com.mobilecard.dtoneservice.client.response.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Amount {
    private Integer base;
    @JsonProperty("promotion_bonus")
    private Integer promotionBonus;
    @JsonProperty("total_excluding_tax")
    private Integer totalExcludingTax;
}
