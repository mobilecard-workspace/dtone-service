package com.mobilecard.dtoneservice.client.response.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Source {
    @JsonProperty("unit_type")
    private String unitType;
    private String unit;
    private BigDecimal amount;
}
