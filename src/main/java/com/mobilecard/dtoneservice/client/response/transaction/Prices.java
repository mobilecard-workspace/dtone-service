package com.mobilecard.dtoneservice.client.response.transaction;

import lombok.Data;

@Data
public class Prices {
    private Wholesale wholesale;
    private Retail retail;
}
