package com.mobilecard.dtoneservice.client.response.transaction;

import lombok.Data;

import java.math.BigInteger;
import java.util.List;

@Data
public class Operator {
    private BigInteger id;
    private String name;
    private Country country;
    private List<Regions> regions;
}
