package com.mobilecard.dtoneservice.client.response.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Sender {
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("middle_name")
    private String middleName;
    @JsonProperty("nationality_country_iso_code")
    private String nationalityCountryIsoCode;
    @JsonProperty("mobile_number")
    private String mobileNumber;
    private String email;
}
