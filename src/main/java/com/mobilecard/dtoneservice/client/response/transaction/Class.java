package com.mobilecard.dtoneservice.client.response.transaction;

import lombok.Data;

@Data
public class Class {
    private Integer id;
    private String message;
}
