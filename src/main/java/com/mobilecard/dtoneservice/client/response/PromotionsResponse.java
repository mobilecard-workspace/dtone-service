package com.mobilecard.dtoneservice.client.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PromotionsResponse {
    private String id;
    private String title;
    private String description;
    private String terms;
    @JsonProperty("start_date")
    private String startDate;
    @JsonProperty("end_date")
    private String endDate;
    private Operator operator;
    private List<Product> products;
}
