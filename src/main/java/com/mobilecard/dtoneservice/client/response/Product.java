package com.mobilecard.dtoneservice.client.response;

import lombok.Data;

@Data
public class Product {
    private String id;
    private String name;
    private String description;
    private String type;
}
