package com.mobilecard.dtoneservice.client.response.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CreditPartyIdentifier {
    @JsonProperty("mobile_number")
    private String mobileNumber;
    @JsonProperty("account_number")
    private String accountNumber;
}
